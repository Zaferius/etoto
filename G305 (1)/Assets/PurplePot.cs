﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PurplePot : MonoBehaviour
{

    private bool Pickable = true; // To avoid players
    public GameManager game;
    public RipplePostProcessor camRipple;


    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player") && Pickable)
        {
            var playercomponent = other.GetComponent<Player>();
            if (playercomponent != null)
            {
                OnPlayerTrigger(playercomponent);
            }
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("Player") && Pickable)
        {
            var playercomponent = other.GetComponent<Player>();
            if (playercomponent != null)
            {
                OnPlayerTrigger(playercomponent);
            }
        }
    }

    void OnPlayerTrigger(Player player)
    {

        Destroy(Instantiate(game.FX_PuprlePot, player.gameObject.transform.position, Quaternion.identity), 0.5f);
        camRipple.RippleEffect();



        Pickable = false;

        // Screenshake
        if (PixelCameraController.instance != null)
        {
            PixelCameraController.instance.Shake(0.1f);
        }

        // Destroy the potion
        Destroy(gameObject);

    }
}
