﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Exit : MonoBehaviour
{

    public Animator myAnimator;
    

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {


    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if (PixelCameraController.instance != null)
            {
                PixelCameraController.instance.Shake(0.1f);
            }
            myAnimator.Play("Disabled");
            Invoke("NextSceneCaller", 1);
           
        }
        
    }

    void NextSceneCaller()
    {
        SceneManager.LoadScene("G");
    }

}

